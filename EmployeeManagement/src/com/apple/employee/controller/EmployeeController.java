package com.apple.employee.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EmployeeController {

	@Autowired
	private EmployeeService service;
	
	@RequestMapping(value= {"/","/home","/index"})
	public ModelAndView home() {
		ModelAndView mav = new ModelAndView();
		System.out.println("Home Controller Called");
		List<Employee> listEmployee = service.listAll();
		mav.addObject("listEmployee",listEmployee);
		mav.addObject("title", "Home");
		mav.addObject("home", true);
		mav.setViewName("page");
		return mav;
	}
	
	@RequestMapping("/new")
	public ModelAndView newEmployee(Map<String, Object> model) {
		model.put("employee", new Employee());
		ModelAndView mav = new ModelAndView();
		mav.addObject("title", "New Employee");
		mav.addObject("userClickNewEmployee", true);
		mav.setViewName("page");
		return mav;
	}
	
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String saveEmployee(@ModelAttribute("employee") Employee employee) {
		System.out.println(employee);
		service.save(employee);
		return "redirect:/";
	}
	
	@RequestMapping(value="/edit")
	public ModelAndView editEmployee(@RequestParam long id) {
		ModelAndView mav = new ModelAndView();
		Employee employee = service.findById(id);
		System.out.println(employee);
		mav.addObject("employee", employee);
		mav.addObject("title", "Edit Employee");
		mav.addObject("userClickEditEmployee", true);
		mav.setViewName("page");
		return mav;
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.GET)
	public String deleteEmployee(@RequestParam long id) {
		service.delete(id);
		return "redirect:/";
	}
	
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public ModelAndView searchEmployee(@RequestParam String keyword) {
		ModelAndView mav = new ModelAndView();
		List<Employee> employee = service.search(keyword);
		mav.addObject("listEmployee", employee);
		mav.addObject("title", "Search Employee");
		mav.addObject("userClickSearchEmployee", true);
		mav.setViewName("page"); 
		return mav;
	}
	
}
