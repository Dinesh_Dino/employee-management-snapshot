package com.apple.employee.controller;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface EmployeeRepository extends CrudRepository<Employee, Long>{
	
	
	@Query(value="SELECT e FROM Employee e WHERE e.name LIKE '%' || :keyword || '%'"
			      +"OR e.age LIKE '%' || :keyword || '%'"
			      +"OR e.designation LIKE '%' || :keyword || '%'"
			      +"OR e.salary LIKE '%' || :keyword || '%'")
	public List<Employee> search(@Param("keyword") String keyword);
		

}
