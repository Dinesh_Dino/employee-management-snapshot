package com.apple.employee.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository repo;

	public List<Employee> listAll() {
		return (List<Employee>) repo.findAll();
	}

	public void save(Employee employee) {
		repo.save(employee);
	}

	public Employee findById(long id) {
		Optional<Employee> employee = repo.findById(id);
		return employee.get();
	}

	public void delete(long id) {
		repo.deleteById(id);
	}

	public List<Employee> search(String keyword) {
		return repo.search(keyword);
	}
}
