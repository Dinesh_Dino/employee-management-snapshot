
<div class="wrapper">

	<div class="container">
		<div class="row">
			<div class="offset-md-2 col-md-8">
				<div class="card border-primary mt-5 shadow-lg">
					<div class="card-header bg-primary text-white text-center">
						<h1>EMPLOYEE MANAGEMENT</h1>
					</div>
					<div class="card-body">
						<form action="search" class="form-row" method="get">
							<div class="col-7">
								<input type="search" class="form-control" name="keyword" />
							</div>
							<div class="col-3">
								<input type="submit" class="form-control btn btn-primary"
									value="Search" />
							</div>
							<div class="col">
								<a href="new" class="btn btn-success"><i class="fa fa-plus"
									aria-hidden="true"></i>Add New</a>
							</div>
						</form>
						<div class="col-md-12 mt-4 p-0">
							<table
								class="table table-bordered table-hover table-responsive-lg text-center">
								<tr>
									<th scope="col">Id</th>
									<th scope="col">Name</th>
									<th scope="col">Age</th>
									<th scope="col">Designation</th>
									<th scope="col">Salary</th>
									<th scope="col" colspan="2">Action</th>
								</tr>
								<c:forEach items="${listEmployee}" var="employee">
									<tr>
										<td>${employee.id}</td>
										<td>${employee.name}</td>
										<td>${employee.age}</td>
										<td>${employee.designation}</td>
										<td>${employee.salary}</td>
										<td><a href="edit?id=${employee.id}"><i
												class="fa fa-pencil-square fa-lg" style="color: orange;"
												aria-hidden="true"></i></a></td>
										<td><a href="delete?id=${employee.id}"><i
												class="fa fa-trash fa-lg" style="color: red;"
												aria-hidden="true"></i></a></td>
									</tr>

								</c:forEach>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

