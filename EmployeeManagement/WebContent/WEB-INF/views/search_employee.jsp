
<div class="wrapper">

	<div class="container">
		<div class="row">
			<div class="offset-md-2 col-md-8">
				<div class="card border-primary mt-5 shadow-lg">
					<div class="card-header bg-primary text-white text-center">
						<h1>SEARCH EMPLOYEE</h1>
					</div>
					<div class="card-body">
						<div class="col-md-12 mt-4 p-0">
							<table
								class="table table-bordered table-hover table-responsive-lg text-center">
								<tr>
									<th scope="col">Id</th>
									<th scope="col">Name</th>
									<th scope="col">Age</th>
									<th scope="col">Designation</th>
									<th scope="col">Salary</th>
								</tr>
								<c:forEach items="${listEmployee}" var="employee">
									<tr>
										<td>${employee.id}</td>
										<td>${employee.name}</td>
										<td>${employee.age}</td>
										<td>${employee.designation}</td>
										<td>${employee.salary}</td>
									</tr>

								</c:forEach>

							</table>
							<a href="home" class="btn btn-success">Home</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

