
<div class="wrapper">

	<div class="container">
		<div class="row">
			<div class="offset-md-2 col-md-8">
				<div class="card border-primary mt-5 shadow-lg">
					<div class="card-header bg-primary text-white text-center">
						<h1>NEW EMPLOYEE</h1>
					</div>
					<div class="card-body">
						<sf:form action="save" class="form-group" method="post"
							modelAttribute="employee">
							<!-- <div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Id</label>
									<div class="col-sm-10">
										<input type="text" class="form-control-plaintext"
											id="inputEmail3" path="id">
									</div>
								</div> -->
							<div class="form-group row">
								<label for="inputPassword3" class="col-sm-2 col-form-label">Name</label>
								<div class="col-sm-10">
									<sf:input type="text" class="form-control" id="inputPassword3"
										path="name" />
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Age</label>
								<div class="col-sm-10">
									<sf:input type="text" class="form-control" id="inputEmail3"
										path="age" />
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Designation</label>
								<div class="col-sm-10">
									<sf:input type="text" class="form-control" id="inputEmail3"
										path="designation" />
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Salary</label>
								<div class="col-sm-10">
									<sf:input type="text" class="form-control" id="inputEmail3"
										path="salary" />
								</div>
							</div>
							<div class="form-group row  col-sm-12">
								<div class="text-left col-6" style="margin-left: 106px;">
									<input type="submit" class="form-control btn btn-primary"
										value="Save" />
								</div>
								<div class="text-right col">
									<a href="home" class="btn btn-success pl-5 pr-5">Home</a>
								</div>
							</div>

						</sf:form>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

